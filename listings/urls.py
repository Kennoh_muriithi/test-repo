from django.conf.urls import url
from . import views
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

schema_view = get_schema_view(
   openapi.Info(
      title="Booking Engine Test API",
      default_version='v1',
      description="API Documentation",
      terms_of_service="/",
      contact=openapi.Contact(email=""),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    url(r'^api/v1/units/',views.GetUnits.as_view(), name='filter_units'),
    url(r'^$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

