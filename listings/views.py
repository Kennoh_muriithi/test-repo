from django.shortcuts import render
from .serializers import *
from rest_framework import generics
from rest_framework.parsers import *
from rest_framework.response import Response
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from django.db.models import Q



class GetUnits(generics.ListAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = ListingSerializer
    parser_classes = [JSONParser]

    @method_decorator(name='get', decorator=swagger_auto_schema(
        operation_id='Fetch excluded reserved listings from blocked dates and max price',
        operation_description=
        """
        An Api View which provides a get method to fetch listings excluded from two reserved dates and a maximum price range filter.
        """))
    def get(self, request):
        max_price = request.GET['max_price']
        check_in = request.GET['check_in']
        check_out = request.GET['check_out']
        queryset = Listing.objects.exclude(block_days__check_in__gte=check_in,block_days__check_out__lte=check_out).filter(
        Q(listing_type='apartment',booking_info__price__lte=max_price) | 
        Q(listing_type='hotel',hotel_room_types__hotel_rooms__room_number__gte=1,booking_info__price__lte=max_price)
        ).distinct().order_by('booking_info__price')
        serializer = ListingSerializer(queryset, many=True)
        return Response(serializer.data)
        