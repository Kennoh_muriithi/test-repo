from .models import *
from rest_framework import serializers


class ListingSerializer(serializers.ModelSerializer):
    price = serializers.DecimalField(max_digits=9,decimal_places=0,source='booking_info.price')

    class Meta:
        model = Listing
        fields = ['listing_type', 'title', 'country', 'city','price']
        